const {DateTime, Settings} = require('luxon');
Settings.defaultZone = 'America/Argentina/Buenos_Aires';
Settings.defaultLocale = 'es';

module.exports = DateTime;
// DateTime.local().setZone('America/Argentina/Buenos_Aires');
// DateTime.setLocale('America/Argentina/Buenos_Aires');
//const d = DateTime.local().setZone('America/Argentina/Buenos_Aires', { keepLocalTime: false });
//console.log(d);
// module.exports = d;
