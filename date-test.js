function isDateBeforeToday(date) {
  return new Date(date.toDateString()) < new Date(new Date().toDateString());
}

console.log(isDateBeforeToday(new Date('2021-10-21')));
