const {legalEntityAccountMapper} = require('../mappers/accounts.mapper');
const {businessMapper} = require('../mappers/business.mapper');
const {businessDetailMapper} = require('../mappers/business_detail.mapper');
const {
  createLegalBusinessSerializer,
} = require('../serializers/legal_entities.serializer');
// const {createAccount} = require('../services/account.service');
// const {createBusiness} = require('../services/business.service');
// const {createBusinessDetail} = require('../services/business_detail.service');
const {Business, Account, BusinessDetail, Branch,PointOfStore, Provider} = require('../models');

exports.createLegalBusiness =async (body) => {
  // const business = createBusiness(businessMapper(body));
  // const account = createAccount(legalEntityAccountMapper(body));
  // const businessDetail = createBusinessDetail(businessDetailMapper(body));
  // const branches = body.branches;

  const business = await Business.create({
    ...businessMapper(body),
    holderLastName: body.customer.first_name,
    partnerName: body.partner,
    business_type: 'QR',
    holderFirstName: body.customer.last_name,
  });
  const account = await Account.create({
    ...legalEntityAccountMapper(body),
    businessId: business.id,
    registredByPartner: false,
    status: 'ACTIVE',
  });

  const businessDetail = await BusinessDetail.create({
    category: body.business_detail.category,
    comercialActivity: body.business_detail.comercial_activity,
    rate: body.business_detail.rate,
    businessName: body.business_detail.business_name,
    fantasyName: body.business_detail.fantasy_name,
    creationDate: body.business_detail.creation_date,
    freeStretch: body.business_detail.free_stretch,
    country: body.business_detail.country,
    city: body.business_detail.city,
    businessId: business.id,
  });

  const branches = await Branch.bulkCreate(
    body.branches.map((branch) => ({
      name: branch.name,
      businessId: business.id,
    })));

  const points = body.branches.map((branch) => {
    const branchId = branches
      .find((newBranch) => newBranch.name === branch.name).id;
    // return {branchId, points: branch.points_of_store};
    return branch.points_of_store.map((point) => ({
      name: point.name,
      branchId,
    }));
  });
  const providers = body.business_detail.providers.map((provider) => ({
    name: provider,
    businessId: business.id,
  }));
  const concatPoints = [].concat(...points);
  await PointOfStore.bulkCreate(concatPoints);
  await Provider.bulkCreate(providers)
    .catch((err) => console.log(err));
  return createLegalBusinessSerializer({
    business,
    account,
    businessDetail,
    branches,
  });
};

exports.getAll = () => {
  return Business.findAll({
    include: [
      {
        model: Account,
        as: 'accounts',
      },
      {
        model: BusinessDetail,
        as: 'detail',
      },
      {
        model: Provider,
        as: 'providers',
      },
      {
        model: Branch,
        as: 'branches',
        include: [
          {
            model: PointOfStore,
            as: 'points',
          },
        ],
      },
    ],
  });
};
